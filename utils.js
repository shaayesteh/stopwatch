function createButton({ text, onClick, color }) {
  const button = document.createElement("button");
  button.innerText = text;
  button.onclick = onClick;
  button.classList.add(color);

  return button;
}

function formatTime(seconds) {
  const hours = Math.floor(seconds / 3600);
  const minutes = Math.floor((seconds % 3600) / 60);
  const secs = seconds % 60;

  return `${String(hours).padStart(2, "0")}:${String(minutes).padStart(
    2,
    "0"
  )}:${String(secs).padStart(2, "0")}`;
}

function append(container, children) {
  if (!Array.isArray(children)) {
    children = [children];
  }

  children.forEach(function (child) {
    container.appendChild(child);
  });
}
