function createStopwatch() {
  const state = createState();

  function createState() {
    return {
      counter: 0,
      intervalId: undefined,
      laps: [],
    };
  }

  console.log(state);

  function render() {
    if (state.intervalId) {
      startButton.innerText = "Pause";
    } else {
      startButton.innerText = "Start";
    }
    display.innerText = formatTime(state.counter);

    lapsContainer.innerHTML = "";
    state.laps.forEach((lapTime, index) => {
      const lapElement = createLap(lapTime, () => resumeFromLap(index), index);
      append(lapsContainer, lapElement);
    });
  }

  function addLap() {
    state.laps.push(state.counter);
    render();
  }

  const lapsContainer = createLapsContainer();

  function createLapsContainer() {
    const container = document.createElement("div");
    container.classList.add("stopwatch__laps");
    return container;
  }

  function createLap(time, onClick, index) {
    const lap = document.createElement("div");
    lap.classList.add("stopwatch__lap");

    const lapTime = document.createElement("span");
    lapTime.innerText = formatTime(time);

    const closeButton = createButton({
      text: "x",
      onClick: function () {
        removeLap(lap);
      },
    });

    const lapLabel = document.createElement("span");
    lapLabel.innerText = `Lap ${index + 1}`;
    lapLabel.classList.add("lap__label");

    append(lap, [lapTime, closeButton, lapLabel]);

    lap.onclick = onClick;
    return lap;
  }

  function reset() {
    clearInterval(state.intervalId);
    state.intervalId = undefined;
    state.counter = 0;
    render();
  }

  function removeLap(lapElement) {
    const lapIndex = Array.from(lapElement.parentNode.children).indexOf(
      lapElement
    );
    state.laps.splice(lapIndex, 1);
    render();
  }

  function resumeFromLap(index) {
    clearInterval(state.intervalId);
    state.intervalId = undefined;
    state.counter = state.laps[index];
    state.laps.splice(index, 1);
    render();
  }

  function pause() {
    clearInterval(state.intervalId);
    state.intervalId = undefined;
    addLap();
    reset();
  }

  function start() {
    state.intervalId = setInterval(() => {
      ++state.counter;
      render();
    }, 1000);
  }

  const startButton = createButton({
    text: "Start",
    onClick: function () {
      state.intervalId ? pause() : start();
    },
    color: "primary",
  });

  const resetButton = createButton({
    text: "Reset",
    onClick: reset,
    color: "secondary",
  });

  const stopwatchButtons = document.createElement("div");
  stopwatchButtons.classList.add("stopwatch__buttons");
  append(stopwatchButtons, [startButton, resetButton]);

  const stopwatch = document.createElement("div");
  stopwatch.classList.add("stopwatch");

  const display = document.createElement("div");
  display.classList.add("stopwatch__display");

  append(stopwatch, [display, lapsContainer, stopwatchButtons]);
  render();

  return stopwatch;
}
