let currentStopwatch;

function setupStopwatch() {
  const stopwatch = createStopwatch();
  currentStopwatch = stopwatch;
  append(document.body, stopwatch);
}

setupStopwatch();

function addLap() {
  if (currentStopwatch) {
    currentStopwatch.addLap();
  }
}
